<?php
declare(strict_types=1);

namespace App\Service\Helpers\Evaluators;

class XmlEvaluatorService
{
    public static function evaluate($rows, $output): int
    {
        $clientWithReadings = [];
        foreach ($rows['reading'] as $row) {
            $clientWithReadings[$row['@clientID']][] = $row;
        }
        $output->writeln('| Client                   | Month          | Suspicious     ');
        $aboveAverage = [];
        foreach ($clientWithReadings as $index => $item) {
            $totalRading = 0;
            foreach ($item as $i) {
                $totalRading += $i['#'];
            }
            $mediaAnual = $totalRading / 12;
            foreach ($item as $i) {
                if ($i['#'] > $mediaAnual) {
                    $aboveAverage[$index][] = $i;
                    $output->writeln(\sprintf('| %s            | %s        | %s ',$index, $i['@period'], $i['#']));
                }
            }
            $output->writeln(\sprintf('Above client: %s total readings: %s', $index, count($aboveAverage[$index])));
        }
        return count($clientWithReadings);
    }
}