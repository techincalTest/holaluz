<?php
declare(strict_types=1);

namespace App\Service\Helpers\Evaluators;

class CsvEvaluatorService
{
    public static function evaluate($rows, $output): int
    {
        $clientWithReadings = [];
        foreach ($rows as $row) {
            $clientWithReadings[$row['client']][] = $row;
        }
        $aboveAverage = [];
        $output->writeln('| Client                   | Month          | Suspicious     ');
        foreach ($clientWithReadings as $index => $item) {
            $totalRading = 0;
            foreach ($item as $i) {
                $totalRading += $i['reading'];
            }
            $mediaAnual = $totalRading / 12;
            foreach ($item as $i) {
                if ($i['reading'] > $mediaAnual) {
                    $aboveAverage[$index][] = $i;
                    $output->writeln(\sprintf('| %s            | %s        | %s ',$index, $i['period'], $i['reading']));
                }
            }
            $output->writeln(\sprintf('Above client: %s total readings: %s', $index, count($aboveAverage[$index])));
        }
        return count($clientWithReadings);

    }
}