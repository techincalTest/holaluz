<?php
declare(strict_types=1);

namespace App\Service\EvaluateReadings;

use App\Service\Helpers\Evaluators\CsvEvaluatorService;
use App\Service\Helpers\Evaluators\XmlEvaluatorService;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ReaderEvaluatorService
{
    public function __construct(readonly string $projectDir)
    {
    }

    public function read($input, $output): int
    {
        $fileToRead = $this->projectDir . '/public/files/' . $input->getArgument('file') .'.'. $input->getArgument('format');
        $decoder = new Serializer([new ObjectNormalizer()], [new CsvEncoder(), new XmlEncoder()]);
        $rows = $decoder->decode(file_get_contents($fileToRead), $input->getArgument('format'));
        return match ($input->getArgument('format')) {
            'csv' => CsvEvaluatorService::evaluate($rows, $output),
            'xml' => XmlEvaluatorService::evaluate($rows, $output),
            default => 0,
        };

    }

}