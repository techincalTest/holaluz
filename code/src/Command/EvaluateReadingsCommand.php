<?php
declare(strict_types=1);

namespace App\Command;

use App\Service\EvaluateReadings\ReaderEvaluatorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EvaluateReadingsCommand extends Command
{
    protected static $defaultName = 'app:evaluate-readings';
    public function __construct(readonly ReaderEvaluatorService $readerEvaluatorService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Evaluate Readings')
            ->addArgument('format', InputArgument::OPTIONAL, 'Formato del fichero a evaluar', 'csv')
            ->addArgument('file', InputArgument::OPTIONAL, 'Nombre del fichero', '2016-readings');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $output->writeln(\sprintf('Total evaluate client: %s ', $this->readerEvaluatorService->read($input, $output)));
        }catch (\Exception $exception){
            $io->writeln($exception->getMessage());
            return Command::FAILURE;
        }
        $io->success('It worked!');
        return Command::SUCCESS;

    }
}