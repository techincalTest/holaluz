<?php
declare(strict_types=1);

namespace App\Tests\Command\Functional;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class EvaluateReadingsCommandTest extends KernelTestCase
{
    public function testExecuteForCsvComand(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:evaluate-readings');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'format' => 'csv',
            'file' => '2016-readings',
        ]);

        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Above client:', $output);
    }

    public function testFailedToOpenFileComand(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('app:evaluate-readings');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'format' => 'xml',
            'file' => '2022-readings',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Failed to open stream:', $output);
    }

    public function testExecuteForCsvWithoutParametersComand(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:evaluate-readings');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $commandTester->assertCommandIsSuccessful();

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Total evaluate client:', $output);
        $this->assertStringContainsString('Above client:', $output);
    }
}