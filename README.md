# HolaLuz



## Crear las imagenes y contenedores
- [ ] make build
- [ ] make run

## Entrar al contendor y ejecutar la aplicacion
- [ ] make ssh-be
- [ ] sf app:evaluate-readings | A este se le pueden pasar dos parametros:
    - [ ] format: formato del fichero, por ahora solo tenemos la opcion de scv y xml
    - [ ] file: nombre del fichero, estos tienen que estar ubicados en el directorio public/file, la idea era poder pasar la ruta por parametro pero no me ha dado tiempo de hacerlo.
    - Ej: 
      - sf app:evaluate-readings xml 
      - sf app:evaluate-readings csv 2016-readings

## Ejecutar los test
Dentro del contenedor ejecutamos:
- [ ] bin/phpunit --filter nombreMetodo
- [ ] bin/phpunit --filter nombreClase
- [ ] Ej:
  - bin/phpunit --filter EvaluateReadingsCommandTest